#Le port sur lequel tourne le serveur
PORT=3035

#Whitelist des noms de domaines d'adresses e-mail qui peuvent accéder à l'appli (utilisé en cas d'usage de WAYF). 
# ex : ["mines-stetienne.fr", "imt.fr"]. Mettre à [] empechera tout utilisateur d'accéder à l'application.
#SHIBBOLETH_DN_WHITELIST=[]

#Utilisateur shibboleth de test, à utiliser tant que shibboleth n'est pas configuré (= pas de protection sur /api/shibboleth/login)
#Décommenter la ligne suivante, et ajuster les valeurs pour l'utiliser. Ne nécessite pas de modifier l'application.
#SHIBBOLETH_TEST_USER={ "eppn": "user@dn.fr", "userId": "username", "email": "user.name@dn.fr", "name": "User Name", "id_koha": "000000000000", "formation": "ELEVE", "service": "ELEVES-NA", "organization": "Organisation" }

#Récupération du numéro de carte via une API externe. Remplacera {uid} par la propriété uid de l'utilisateur connecté
SHIBBOLETH_RUCCARTE_API=https://api.example.com/uid/{uid}/

#L'adresse front, nécessairement visible depuis internet, qui servira de proxy pour les images
SERVER_URL=https://domain.local/

#Utilisé pour le proxy des images vers GRR, qui est certainement accessible uniquement au sein du réseau école
GRR_URL=https://grr.example.com/

#Utilisé pour alerter des administrateurs, sur les salles et matériel nécessitant une modération, qu'une réservation a été effectuée
#GRR_MODERATION_RESERVATION_MAIL=alias@example.com

#Type de réservation par défaut pour les réservations rapides, parmi celles déclarées dans admin/admin_type.php
GRR_DEFAULT_RESERVATION_TYPE=A

#Adresse du endpoint de Prismic, pour les actualités et descriptions
PRISMIC_URL=https://subdomain.prismic.io/api/v2

#Adresse du module libre service de Koha
KOHA_URL=https://www.example.com/cgi-bin/koha/sco/sco-main.pl

#Numéro de version de Koha. Doit matcher avec un fichier JSON dans /api/koha/error_codes
# (rechercher la balise meta "generator" sur le site pour connaitre ce numéro de version)
KOHA_VERSION=19.1112000


#Cas où la page de module libre service est intégrée dans une autre page en iframe
#Renseigner le même nom de domaine que KOHA_URL
#KOHA_REFERER_URL=https://www.example.com

#Cas où la page de module libre service est protégée par un login / mot de passe
#Renseigner des identifiants longue durée, qui seront utilisés pour les prêts de tous les utilisateurs de l'app.
#KOHA_AUTHWALL_LOGIN=
#KOHA_AUTHWALL_PASSWORD=

#Cas spécifique, où un préfixe ou un suffixe doit être ajouté à tous les code barres.
#KOHA_BARCODE_PREFIX=
#KOHA_BARCODE_SUFFIX=

#Adesses des fichiers ICS (via zimbra par exemple)
AGENDA_ICS_URL=https://zimbra.example.com/home/user@domain.fr/Agenda.ics
HORAIRES_ICS_URLS=[{ "campus_id": 1, "campus_code": "FR", "url": "https://zimbra.example.com/home/user@domain.fr/Agenda.ics"}]

#Le SMTP (sans authentification) nécessaire à l'envoi de mail
SMTP_HOST=smtp://smtp.example.fr
#Adresse mail utilisée comme expéditrice
SMTP_FROM=noreply@example.fr

#Les informations de connexion à la base de données GRR (les connexions doivent être autorisées depuis ce host)
MYSQL_HOST=localhost
MYSQL_USER=root
MYSQL_PASSWORD=toor
MYSQL_PORT=3306
MYSQL_DATABASE=grr
MYSQL_WAITFORCONNECTIONS=true
MYSQL_CONNECTIONLIMIT=10
MYSQL_QUEUELIMIT=0

#Pour la récupération sélectives des éléments de GRR. Mettre [] pour tous, ou [number, ...]
IDS_AREA_MATERIEL=[]
IDS_AREA_SALLES=[]
#Pour empêcher la récupération de salle, basé sur une recherche textuelle sur le nom de la salle. Ex : ["Cabane verte"]. Laisser vide si aucun usage.
BLACKLIST_SALLE_NAME=[]

#Adresses mails de contacts
SUGGESTION_ALIAS_MAIL_CATEGORIES={"1": "services@example.com", "2" : "ressources@example.com", "3" : "apprentissage-enseignement@example.com", "4" : "outils-pedagogiques@example.com", "5" : "lieux-apprentissage@example.com", "6" : "application-intranet@example.com", "7" : "autres@example.com"}
RDV_ALIAS_MAIL_DESTINATAIRE={"1" : "conseiller-pedagogique@example.com", "2" : "ingenieur-pedagogique@example.com", "3" : "documentaliste@example.com", "4" : "conseiller-juridique@example.com", "5" : "technicien-video@example.com", "6" : "direction@example.com"}
DEMANDE_REFERENCE_ALIAS_MAIL=documentaliste@example.com

#Adresse e-mail affichée en cas d'erreur de connexion shibboleth, pour que l'utilisateur contacte le support en cas d'erreur
SUPPORT_TECHNIQUE_MAIL=dsi@example.com

#Des meta-informations incrustées par défaut sur les pages générées par le serveur, utilisée par Prismic
SEO_TITLE=LCA
SEO_DESCRIPTION=LCA
SEO_IMAGE=